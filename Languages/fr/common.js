{
	"date_format" : "d/m/Y",
	"time_format" : "H:i",
	
	"home" : "Accueil",
	"search" : "Jeux",
	"manager" : "Gestionnaire",
	"forums" : "Forums",
	"help" : "Aide",
	"about" : "À propos",
	
	"copyright_aao" : "Ace Attorney Online © Unas & Spparrow 2006–<year>. Interdiction de recopier du contenu de ce site sans l'autorisation du webmaster.",
	"copyright_capcom" : "Ace Attorney® et les propriétés associées appartiennent à Capcom, Ltd. Ace Attorney Online n'est pas associé à Capcom, et n'est maintenu que par des fans.",
	"disclaimer_title" : "Disclaimer",
	"disclaimer_text" : "Ce site met à disposition des outils à but créatif. Il ne dispense pas d'acheter les jeux Ace Attorney.",
	
	"profile_judge" : "Juge"
}
