{
"trial_search" : "Recherche",
"trial_search_results" : "Résultats de recherche",
"trial_search_featured" : "Procès coups de cœur",

"trial_search_current" : "Recherche en cours",
"trial_search_add" : "Ajouter un critère",

"trial_search_title" : "Titre",
"trial_search_language" : "Langue",
"trial_search_authorId" : "ID de l'auteur",
"trial_search_authorName" : "Nom de l'auteur",
"trial_search_featuredTrial" : "Coup de cœur",
"trial_search_featuredTrial_isTrue" : "Est dans les coups de cœur",
"trial_search_featuredTrial_isFalse" : "N'est pas dans les coups de cœur",
"trial_search_sequenceId" : "ID de la série",
"trial_search_collabsName" : "Noms des collaborateurs",
"trial_search_collabsId" : "IDs des collaborateurs",
"trial_search_authorOrCollabsId" : "IDs de l'auteur ou des collaborateurs",

"trial_search_is" : "est exactement",
"trial_search_isNot" : "n'est pas",
"trial_search_contains" : "contient",
"trial_search_doesNotContain" : "ne contient pas",
"trial_search_has" : "inclut exactement",
"trial_search_doesNotHave" : "n'inclut pas",
"trial_search_hasValueContaining" : "contiennent",
"trial_search_doesNotHaveValueContaining" : "ne contiennent pas",

"trial_search_private" : "Montrer les procès non publiés",

"trial_search_page" : "Page"
}
