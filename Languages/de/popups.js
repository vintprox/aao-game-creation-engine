{
"default_popups_fouet" : "Charakterspezifisch|Peitsche",
"default_popups_fouet-inversee" : "Charakterspezifisch|Peitsche (rückwärts)",

"default_popups_guilty" : "Events|Schuldig (ENG)",
"default_popups_cross-examination" : "Events|Kreuzverhör (ENG)",
"default_popups_not-guilty" : "Events|Nicht Schuldig (ENG)",
"default_popups_witness-testimony" : "Events|Zeugenaussage (ENG)",
"default_popups_testimony-statement" : "Events|Aussage - Statement (ENG)",
"default_popups_game-over-doors" : "Events|Game-Over-Türen",
"default_popups_unlock-successful" : "Events|Blockade gelöst (ENG)",

"default_popups_eureka" : "Bubbles|Eureka!",
"default_popups_gotcha" : "Bubbles|Ertappt! (ENG)",
"default_popups_hold-it" : "Bubbles|Moment mal! (ENG)",
"default_popups_not-so-fast" : "Bubbles|Nicht so schnell! (ENG)",
"default_popups_objection" : "Bubbles|Einspruch! (ENG)",
"default_popups_take-that" : "Bubbles|Nimm das! (ENG)",

"default_popups_contre-interrogatoire" : "Französische Grafiken|Kreuzverhör",
"default_popups_j-te-tiens" : "Französische Grafiken|Ertappt!",
"default_popups_coupable" : "Französische Grafiken|Schuldig",
"default_popups_un-instant" : "Französische Grafiken|Moment mal!",
"default_popups_non-coupable" : "Französische Grafiken|Nicht schuldig",
"default_popups_prends-ca" : "Französische Grafiken|Nimm das!",
"default_popups_deposition-du-temoin" : "Französische Grafiken|Zeugenaussage",

"default_popups_aussage" : "Deutsche Grafiken|Aussage - Statement",
"default_popups_einspruch" : "Deutsche Grafiken|Einspruch!",
"default_popups_ertappt" : "Deutsche Grafiken|Ertappt!",
"default_popups_kreuzverhor" : "Deutsche Grafiken|Kreuzverhör",
"default_popups_moment-mal" : "Deutsche Grafiken|Moment mal!",
"default_popups_nimm-das" : "Deutsche Grafiken|Nimm das!",
"default_popups_schuldig" : "Deutsche Grafiken|Schuldig",
"default_popups_zeugenaussage" : "Deutsche Grafiken|Zeugenaussage",

"default_popups_te-tengo" : "Spanische Grafiken|Ertappt!",
"default_popups_un-momento" : "Spanische Grafiken|Moment mal!",
"default_popups_protesto" : "Spanische Grafiken|Einspruch!",
"default_popups_toma-ya" : "Spanische Grafiken|Nimm das!"
}
